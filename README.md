# Description

This is an NPM package for building OPC-UA enabled applications with the 
Pimoroni Automation HAT.

[![Donations][badge:donate:image]][badge:donate:link]
[![pipeline](https://gitlab.com/MartinPetersenDev/automationhat-opcua/badges/master/pipeline.svg)](https://gitlab.com/MartinPetersenDev/automationhat-opcua)
[![coverage](https://gitlab.com/MartinPetersenDev/automationhat-opcua/-/raw/master/coverage/badge.svg)](https://gitlab.com/MartinPetersenDev/automationhat-opcua)
[![npm version](https://badge.fury.io/js/automationhat-opcua.svg)](https://badge.fury.io/js/automationhat-opcua)

# Installation

```javascript
npm i --save automationhat-opcua
```

# Usage

The following example creates creates an OPC-UA server called
'AutomationHAT' and runs the myApp function every 500 milliseconds.

GPIO, ADC and the content of the 'variables' object is exposed to
OPC-UA.

```javascript
'use strict'
const hat = require('automationhat-opcua');

// My OPC variables
const variables = { resetCounter1: 0 };

// My application
const myApp = (app_var) => {
    // Local variables copy
    const app = { ...app_var };

    // Log message    
    console.log(`MyApp was called! App.resetCounter1 has value ${app.resetCounter1}`);

    // Reset counter1
    if (app.resetCounter1 === 1) {
        hat.gpio.resetCounter(hat.pins.gpio.input1);
        app.resetCounter1 = 0;
    }

    // Return updated application variables
    return app;
}

// Start OPC-UA server and run myApp
hat.run('AutomationHAT', myApp, variables, 500);
```

[badge:donate:image]: https://liberapay.com/assets/widgets/donate.svg
[badge:donate:link]: https://liberapay.com/MartinPetersenDev/donate
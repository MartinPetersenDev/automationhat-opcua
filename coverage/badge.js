const lcov2badge = require('lcov2badge');
lcov2badge.badge('./coverage/lcov.info', function (err, svgBadge) {
    if (err) throw err;
    console.log(svgBadge);
});

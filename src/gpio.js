/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';
const Gpio = require('onoff').Gpio;
const pins = require('./pins');
const sn3218 = require('./sn3218');

// Globals
const DEBOUNCE = 10;
const DEBUG = false;
const relay1 = new Gpio(pins.gpio.relay1, 'out');
const relay2 = new Gpio(pins.gpio.relay2, 'out');
const relay3 = new Gpio(pins.gpio.relay3, 'out');
const output1 = new Gpio(pins.gpio.output1, 'out');
const output2 = new Gpio(pins.gpio.output2, 'out');
const output3 = new Gpio(pins.gpio.output3, 'out');

/**
 * Input object
 */
const Input = (pin) => {
    const obj = {
        value: false,
        count: 0,
    };

    const input = new Gpio(pin, 'in', 'both', { debounceTimeout: DEBOUNCE });

    /* istanbul ignore next */
    process.on('SIGINT', (_) => {
        input.unexport();
    });

    input.watch((err, value) => {
        obj.value = !!(!err && value === 1);
        value && obj.count++;
        DEBUG &&
            console.log(
                `Input pin(${pin}): value=${obj.value} count=${obj.count}`
            );
    });

    obj.reset = resetCallback();

    return obj;
};

const resetCallback = () => {
    return function () {
        this.count = 0;
        return this;
    };
};

const input1 = Input(pins.gpio.input1);
const input2 = Input(pins.gpio.input2);
const input3 = Input(pins.gpio.input3);

/**
 * Reset outputs
 */
const resetOutputs = () => {
    write(pins.gpio.relay1, 0);
    write(pins.gpio.relay2, 0);
    write(pins.gpio.relay3, 0);
    write(pins.gpio.output1, 0);
    write(pins.gpio.output2, 0);
    write(pins.gpio.output3, 0);
    return 0;
};

/**
 * Write output
 * @param {*} output - output according to pins.gpio
 * @param {*} _value - [0|1|true|false]
 */
const write = (output, value) => {
    let _value;

    // Cast boolean as number
    if (typeof value === 'boolean') {
        _value = value ? 1 : 0;
    } else {
        _value = value;
    }

    if (_value !== 1 && _value !== 0) {
        console.error(`Unknown value ${_value} of type ${typeof _value}`);
        return 1; // Unknown value
    } else if (output === pins.gpio.relay1) {
        relay1.writeSync(_value);
        sn3218.set(pins.led.relay1no, _value === 0 ? 1 : 0);
        sn3218.set(pins.led.relay1nc, _value === 1 ? 1 : 0);
        DEBUG && console.log(`Relay1: ${_value ? 'on' : 'off'}`);
        return 0;
    } else if (output === pins.gpio.relay2) {
        relay2.writeSync(_value);
        sn3218.set(pins.led.relay2no, _value === 0 ? 1 : 0);
        sn3218.set(pins.led.relay2nc, _value === 1 ? 1 : 0);
        DEBUG && console.log(`Relay2: ${_value ? 'on' : 'off'}`);
        return 0;
    } else if (output === pins.gpio.relay3) {
        relay3.writeSync(_value);
        sn3218.set(pins.led.relay3no, _value === 0 ? 1 : 0);
        sn3218.set(pins.led.relay3nc, _value === 1 ? 1 : 0);
        DEBUG && console.log(`Relay3: ${_value ? 'on' : 'off'}`);
        return 0;
    } else if (output === pins.gpio.output1) {
        output1.writeSync(_value);
        sn3218.set(pins.led.output1, _value === 1 ? 1 : 0);
        DEBUG && console.log(`Output1: ${_value ? 'on' : 'off'}`);
        return 0;
    } else if (output === pins.gpio.output2) {
        output2.writeSync(_value);
        sn3218.set(pins.led.output2, _value === 1 ? 1 : 0);
        DEBUG && console.log(`Output2: ${_value ? 'on' : 'off'}`);
        return 0;
    } else if (output === pins.gpio.output3) {
        output3.writeSync(_value);
        sn3218.set(pins.led.output3, _value === 1 ? 1 : 0);
        DEBUG && console.log(`Output3: ${_value ? 'on' : 'off'}`);
        return 0;
    } else {
        console.error(`Unknown output ${output} of type ${typeof output}`);
        return 2; // Unknown output
    }
};

/**
 * Read input
 *
 * @param {*} input - input according to pins.gpio
 * @returns {Object} - {value: Boolean, count: Number}
 */
const read = (input) => {
    if (input === pins.gpio.input1) {
        sn3218.set(pins.led.input1, input1.value ? 1 : 0);
        return { value: input1.value, count: input1.count };
    } else if (input === pins.gpio.input2) {
        sn3218.set(pins.led.input2, input2.value ? 1 : 0);
        return { value: input2.value, count: input2.count };
    } else if (input === pins.gpio.input3) {
        sn3218.set(pins.led.input3, input3.value ? 1 : 0);
        return { value: input3.value, count: input3.count };
    } else {
        throw new Error(`Unknown input ${input} of type ${typeof input}`);
    }
};

/**
 * Reset counter
 */
const resetCounter = (input) => {
    if (input === pins.gpio.input1) {
        input1.reset();
        return 0;
    } else if (input === pins.gpio.input2) {
        input2.reset();
        return 0;
    } else if (input === pins.gpio.input3) {
        input3.reset();
        return 0;
    } else {
        console.error(`Unknown counter ${input} of type ${typeof input}`);
        return 2;
    }
};

/**
 * Unexport on exit
 */
/* istanbul ignore next */
process.on('SIGINT', (_) => {
    output1.unexport();
    output2.unexport();
    output3.unexport();
    relay1.unexport();
    relay2.unexport();
    relay3.unexport();
});

module.exports.resetOutputs = resetOutputs;
module.exports.resetCounter = resetCounter;
module.exports.write = write;
module.exports.read = read;

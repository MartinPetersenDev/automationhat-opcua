/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

const os = require('os');
const opcua = require('node-opcua');
const appWrapper = require('./appWrapper');

/**
 * Start OPC-UA server and run application at interval
 * @param {String} resourcename - OPC-UA resource name
 * @param {Function} application - Application Code
 * @param {Number} interval - milliseconds
 * @param {Array} variables - Array of application specific variables
 */
const run = async (resourcename, application, variables, interval) => {
    try {
        const server = new opcua.OPCUAServer({
            port: 4334,
            resourcePath: `/UA/${resourcename}`,
            buildInfo: {
                productName: resourcename,
                buildNumber: '1.0.0',
                buildDate: new Date(2020, 10, 10),
            },
        });

        await server.initialize();

        const { addressSpace } = server.engine;
        const namespace = addressSpace.getOwnNamespace();

        const device = namespace.addObject({
            organizedBy: addressSpace.rootFolder.objects,
            browseName: resourcename,
        });

        namespace.addVariable({
            componentOf: device,
            nodeId: 's=used_memory',
            browseName: 'UsedMemory',
            dataType: 'Double',
            value: {
                get() {
                    return new opcua.Variant({
                        dataType: opcua.DataType.Double,
                        value: usedMemory(),
                    });
                },
            },
        });

        // Create and initialize application wrapper
        let app = appWrapper.app(variables);
        app = app.run(application);

        // Poll application at fixed intervals
        setInterval(async () => {
            app = await app.run(application);
        }, interval);

        // Create OPC-UA variables from app.variables
        Object.keys(app.variables).map((key) => {
            namespace.addVariable({
                componentOf: device,
                browseName: key,
                dataType: 'Double',
                value: {
                    get() {
                        return new opcua.Variant({
                            dataType: opcua.DataType.Double,
                            value: app.variables[key],
                        });
                    },
                    set(variant) {
                        app.variables[key] = parseFloat(variant.value);
                        return opcua.StatusCodes.Good;
                    },
                },
            });
        });

        await server.start();
        console.log('OPC-UA server is listening ( press CTRL+C to stop)');
        const { endpointUrl } = server.endpoints[0].endpointDescriptions()[0];
        console.log('... primary server endpoint url is', endpointUrl);
    } catch (err) {
        console.log(err);
    }
};

/**
 * returns the percentage of used memory on the running machine
 * @return {double}
 */
function usedMemory() {
    const percentageMemUsed =
        ((os.totalmem() - os.freemem()) / os.totalmem()) * 100.0;

    return percentageMemUsed;
}

module.exports.run = run;

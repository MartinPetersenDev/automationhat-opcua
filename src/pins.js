/**
 * Copyright (c) 2020 Martin Petersen / @MartInput etersenDev
 */

'use strict';

// Pinout reference from
// https://github.com/pimoroni/automation-hat/blob/master/library/automationhat/__init__.py

// SN3218 LEDs
const led = {
    adc1: 1,
    adc2: 2,
    adc3: 3,
    output1: 4,
    output2: 5,
    output3: 6,
    relay1no: 7,
    relay1nc: 8,
    relay2no: 9,
    relay2nc: 10,
    relay3no: 11,
    relay3nc: 12,
    input1: 15,
    input2: 14,
    input3: 13,
    warn: 16,
    comms: 17,
    power: 18,
};
Object.freeze(led);

// GPIOs
const gpio = {
    relay1: 13,
    relay2: 19,
    relay3: 16,
    output1: 5,
    output2: 12,
    output3: 6,
    input1: 26,
    input2: 20,
    input3: 21,
};
Object.freeze(gpio);

// ADC
const adc = {
    analog1: 0,
    analog2: 1,
    analog3: 2,
    analog4: 3,
};
Object.freeze(adc);

module.exports.led = led;
module.exports.gpio = gpio;
module.exports.adc = adc;

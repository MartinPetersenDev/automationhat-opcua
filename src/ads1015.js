/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

const i2c = require('i2c-bus');
const utils = require('./utils');

// Globals
const ADR = 0x48;
const REG_CONV = 0x00;
const REG_CFG = 0x01;
const SAMPLES_PER_SECOND = 0x0080; // 1600
const CHANNEL_MAP = [0x4000, 0x5000, 0x6000, 0x7000];
const PGA = 0x0200; // FS
const FS = 4096; // don't change
const VCC = 3300; // don't change
const MAX = parseFloat((VCC / FS) * 0x7ff0, 10);
const SINGLE_SHOT = 0x8000;

/**
 * Read single-shot value from channel
 * @param {Number} - channel (0-4)
 * @returns {Number} - value
 */
function read(channel) {
    if (channel < 0 || channel > 4) {
        throw new Error(
            `Unknown analog channel ${channel} of type ${typeof channel}`
        );
    }

    // Setup
    const CONFIG =
        0x0003 |
        0x0100 |
        SAMPLES_PER_SECOND |
        CHANNEL_MAP[channel] |
        PGA |
        SINGLE_SHOT;
    const cfgBuffer = Buffer.from([(CONFIG >> 8) & 0xff, CONFIG & 0xff]);
    const convBuffer = Buffer.from([0, 0]);

    // Read
    const i2c1 = i2c.openSync(1);
    i2c1.writeI2cBlockSync(ADR, REG_CFG, cfgBuffer.length, cfgBuffer);

    // Ugly sync blocking hack
    for (let i = 0; i < 1e6; i++) {
        // nop
    }
    // await utils.sleep(1 / 1600 + 0.0002);

    i2c1.readI2cBlockSync(ADR, REG_CONV, cfgBuffer.length, convBuffer);
    i2c1.closeSync();

    // Convert
    let raw = (convBuffer[0] << 8) | convBuffer[1];
    if (raw & 0x800) {
        raw -= 1 << 12;
    }
    const value = (raw / MAX) * 25.85; // voltage divider circuit

    return value;
}

module.exports.read = read;

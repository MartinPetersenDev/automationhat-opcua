/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const opcserver = require('./opcserver');
const sn3218 = require('./sn3218');
const gpio = require('./gpio');
const pins = require('./pins');
const ads1015 = require('./ads1015');

/**
 * Start AutomationHAT OPC-UA server and run application at interval
 * @param {String} resourcename - OPC-UA resource name
 * @param {Function} application - Application Code
 * @param {Number} interval - milliseconds
 * @param {Object} variables - Array of application specific variables
 */
async function run(resourcename, application, variables = {}, interval = 1000) {
    // Error checks
    try {
        if (resourcename === undefined)
            throw new Error('Resourcename cannot be undefined');
        if (application === undefined)
            throw new Error('Application cannot be undefined');
        if (typeof application !== 'function')
            throw new Error('Application must be a function');
    } catch (error) {
        console.log(error);
        process.exit(1);
    }

    // Flash them blinken lights and go!
    const retval = await sn3218.ledCheck();
    if (retval === 0) {
        gpio.resetOutputs();
        opcserver.run(resourcename, application, variables, interval);
    }
}

module.exports.run = run;
module.exports.gpio = gpio;
module.exports.pins = pins;
module.exports.sn3218 = sn3218;
module.exports.ads1015 = ads1015;

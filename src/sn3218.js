/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';
const i2c = require('i2c-bus');
const pins = require('./pins');
const utils = require('./utils');

// Globals
const ADR = 0x54;
const RESET = 0x17;
const ENABLE = 0x00;
const UPDATE = 0x16;
const LEDS = 0x13;
const PWM = 0x01;
let buf = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

/**
 * Set led in buffer, and return new buffer
 * @param {buffer} - Existing buffer
 * @param {led} - LED # to modify, range 1-18
 * @param {value} - New value for led
 * @returns {buffer}
 */
const led = (buffer, led, value) => {
    const buf = Buffer.from([...buffer]);
    buf[led - 1] = value;
    return led >= 1 && led <= buf.length ? buf : undefined;
};

/**
 * Disables and resets all outputs
 * @returns {Number} - Returns zero
 */
const reset = () => {
    disable();
    const i2c1 = i2c.openSync(1);
    i2c1.writeByteSync(ADR, RESET, 1);
    i2c1.writeByteSync(ADR, RESET, 0);
    i2c1.closeSync();
    return 0;
};

/**
 * Enable SN3218
 * @returns {Number} - Returns zero
 */
const enable = () => {
    const i2c1 = i2c.openSync(1);
    i2c1.writeByteSync(ADR, ENABLE, 1);
    i2c1.closeSync();
    return 0;
};

/**
 * Disable SN3218
 *  @returns {Number} - Returns zero
 */
const disable = () => {
    const i2c1 = i2c.openSync(1);
    i2c1.writeByteSync(ADR, ENABLE, 0);
    i2c1.closeSync();
    return 0;
};

/**
 * Initialize SN3218, all LEDS set to off
 * @returns {Number} - Returns zero
 */
const init = () => {
    reset();
    enable();
    const ledBuffer = Buffer.from([0x3f, 0x3f, 0x3f]);
    const pwmBuffer = Buffer.from([
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    ]);
    const i2c1 = i2c.openSync(1);
    i2c1.writeI2cBlockSync(ADR, LEDS, ledBuffer.length, ledBuffer);
    i2c1.writeI2cBlockSync(ADR, PWM, pwmBuffer.length, pwmBuffer);
    i2c1.writeByteSync(ADR, UPDATE, 0x01);
    i2c1.closeSync();
    return 0;
};

/**
 * Set output # on SN3218
 *
 * @param {Number} ledno - LED # in range 1-18
 * @param {Number} value - 1=turn on else turn off
 * @returns {Number} - Returns zero
 */
const set = (ledno, value) => {
    buf = led(buf, ledno, value === 1 ? 1 : 0);

    const i2c1 = i2c.openSync(1);
    i2c1.writeI2cBlockSync(ADR, PWM, buf.length, buf);
    i2c1.writeByteSync(ADR, UPDATE, 0x01);
    i2c1.closeSync();
    return 0;
};

/**
 * Turn on power led
 */
const powerOn = () => {
    set(pins.led.power, 1);
    return 0;
};

/**
 * Turn on comms led
 */
const commsOn = () => {
    set(pins.led.comms, 1);
    return 0;
};

/**
 * Run through all LED's ... all fun and games
 *
 * @returns {Number} - Returns zero
 */
async function ledCheck() {
    /* istanbul ignore next */
    const ledOrder = [
        1,
        2,
        3,
        4,
        5,
        6,
        15,
        14,
        13,
        7,
        8,
        9,
        10,
        11,
        12,
        16,
        17,
        18,
    ];

    init();

    for (let i = 0; i < 18; i++) {
        set(ledOrder[i], 1);
        await utils.sleep(50);
    }

    for (let i = 17; i >= 0; i--) {
        set(ledOrder[i], 0);
        await utils.sleep(50);
    }

    await utils.sleep(100);
    powerOn();
    await utils.sleep(100);
    commsOn();

    return 0;
}

/**
 * Reset on exit
 */
/* istanbul ignore next */
process.on('SIGINT', (_) => {
    reset();
});

module.exports.led = led;
module.exports.init = init;
module.exports.set = set;
module.exports.reset = reset;
module.exports.ledCheck = ledCheck;
module.exports.powerOn = powerOn;
module.exports.commsOn = commsOn;

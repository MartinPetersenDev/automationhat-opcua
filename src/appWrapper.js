/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const sn3218 = require('./sn3218');
const gpio = require('./gpio');
const pins = require('./pins');
const ads1015 = require('./ads1015');

/**
 * Builds application wrapper type
 * @return {Object} application
 */
const applicationType = (variables) => {
    const app = {
        variables: {
            relay1: false,
            relay2: false,
            relay3: false,
            input1: false,
            input2: false,
            input3: false,
            counter1: 0,
            counter2: 0,
            counter3: 0,
            output1: false,
            output2: false,
            output3: false,
            adc1: 0,
            adc2: 0,
            adc3: 0,
            ...variables,
        },
    };

    app.run = run();
    // app.init = init();

    return app;
};

/**
 * Run application wrapper and call application code
 * */
const run = () => {
    return function (application) {
        // Read inputs
        const _raw1 = gpio.read(pins.gpio.input1);
        const _raw2 = gpio.read(pins.gpio.input2);
        const _raw3 = gpio.read(pins.gpio.input3);
        this.variables.input1 = _raw1.value;
        this.variables.counter1 = _raw1.count;
        this.variables.input2 = _raw2.value;
        this.variables.counter2 = _raw2.count;
        this.variables.input3 = _raw3.value;
        this.variables.counter3 = _raw3.count;
        this.variables.adc1 = ads1015.read(pins.adc.analog1);
        this.variables.adc2 = ads1015.read(pins.adc.analog2);
        this.variables.adc3 = ads1015.read(pins.adc.analog3);

        // Execute application code
        this.variables = application(this.variables);

        // Write outputs
        gpio.write(pins.gpio.relay1, this.variables.relay1);
        gpio.write(pins.gpio.relay2, this.variables.relay2);
        gpio.write(pins.gpio.relay3, this.variables.relay3);
        gpio.write(pins.gpio.output1, this.variables.output1);
        gpio.write(pins.gpio.output2, this.variables.output2);
        gpio.write(pins.gpio.output3, this.variables.output3);

        return this;
    };
};

/**
 * Export application wrapper
 * */
module.exports.app = function (variables) {
    // Respond to SIGINT
    process.on('SIGINT', () => process.exit(0));

    // Respond to SIGTERM
    process.on('SIGTERM', () => process.exit(0));

    // Initialize hardware
    sn3218.init();
    gpio.resetOutputs();
    sn3218.powerOn();
    sn3218.commsOn();

    // Instantiate new app
    return applicationType(variables);
};

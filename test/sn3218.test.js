/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

describe('SN3218 test', function () {
    const sn3218 = require('../src/sn3218');

    it('led() whan passed led = 0 should return undefined', function () {
        const buffer = Buffer.from([0, 0, 0, 0]);
        const result = sn3218.led(buffer, 0, 1);
        assert.strictEqual(result, undefined);
    });

    it('led() whan passed led < 0 should return undefined', function () {
        const buffer = Buffer.from([0, 0, 0, 0]);
        const result = sn3218.led(buffer, -1, 1);
        assert.strictEqual(result, undefined);
    });

    it('led() whan passed led > buf.length should return undefined', function () {
        const buffer = Buffer.from([0, 0, 0, 0]);
        const result = sn3218.led(buffer, 5, 1);
        assert.strictEqual(result, undefined);
    });

    it('led() whan passed led=n, value=1 should return buffer with index[n]=1', function () {
        let buffer = Buffer.from([0, 0, 0, 0]);
        const expected = Buffer.from([0, 0, 0, 0]);

        for (let i = 0; i < 5; i++) {
            assert.deepEqual(buffer, expected);
            expected[i] = 1;
            buffer = sn3218.led(buffer, i + 1, 1); // led() is not zero indexed
        }
    });

    it('led() whan passed led=n, value=0 should return buffer with index[n]=0', function () {
        let buffer = Buffer.from([1, 1, 1, 1]);
        const expected = Buffer.from([1, 1, 1, 1]);

        for (let i = 0; i < 5; i++) {
            assert.deepEqual(buffer, expected);
            expected[i] = 0;
            buffer = sn3218.led(buffer, i + 1, 0); // led() is not zero indexed
        }
    });

    it('init() returns 0', function () {
        const result = sn3218.init();
        assert.strictEqual(result, 0);
    });

    it('reset() returns 0', function () {
        const result = sn3218.reset();
        assert.strictEqual(result, 0);
    });
});

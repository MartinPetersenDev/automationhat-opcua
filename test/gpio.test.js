/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

describe('gpio test', function () {
    const gpio = require('../src/gpio');
    const pins = require('../src/pins');
    const sinon = require('sinon');

    // sinon.stub(gpio.output1, "writeSync").returns(0); ???

    it('write() returns 1 when passed unknown string value', function () {
        const retval = gpio.write(pins.gpio.output1, 'true');
        assert.strictEqual(retval, 1);
    });

    it('write() returns 1 when passed invalid numeric value', function () {
        const retval = gpio.write(pins.gpio.output1, 3);
        assert.strictEqual(retval, 1);
    });

    it('write() returns 2 when passed unknown output', function () {
        const retval = gpio.write(32, 1);
        assert.strictEqual(retval, 2);
    });

    it('write() returns 0 when passed valid number value and output', function () {
        const retval = gpio.write(pins.gpio.output1, 0);
        assert.strictEqual(retval, 0);
    });

    it('write() returns 0 when passed valid boolean value and output', function () {
        const retval = gpio.write(pins.gpio.output1, true);
        assert.strictEqual(retval, 0);
    });

    it('resetOutputs() returns 0', function () {
        const retval = gpio.resetOutputs();
        assert.strictEqual(retval, 0);
    });

    it('read(pins.gpio.input1) returns true or false', function () {
        const retval = gpio.read(pins.gpio.input1);
        const result = retval !== undefined;
        assert.strictEqual(result, true);
    });

    it('read(pins.gpio.input2) returns true or false', function () {
        const retval = gpio.read(pins.gpio.input2);
        const result = retval !== undefined;
        assert.strictEqual(result, true);
    });

    it('read(pins.gpio.input3) returns true or false', function () {
        const retval = gpio.read(pins.gpio.input3);
        const result = retval !== undefined;
        assert.strictEqual(result, true);
    });

    it('resetCounter(pins.gpio.input2) returns 0', function () {
        assert.strictEqual(gpio.resetCounter(pins.gpio.input2), 0);
    });

    it('resetCounter(pins.gpio.input3) returns 0', function () {
        assert.strictEqual(gpio.resetCounter(pins.gpio.input3), 0);
    });

    it('resetCounter(-1) returns 2', function () {
        assert.strictEqual(gpio.resetCounter(-1), 2);
    });

    it('read() throws error when passed invalid input -1', function () {
        assert.throws(function () {
            gpio.read(-1);
        }, Error);
    });
});

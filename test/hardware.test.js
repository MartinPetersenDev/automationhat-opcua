/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');
const { doesNotMatch } = require('assert');

describe('gpio test', function () {
    const sinon = require('sinon');
    const gpio = require('../src/gpio');
    const pins = require('../src/pins');
    const utils = require('../src/utils');
    const ads1015 = require('../src/ads1015');
    const sn3218 = require('../src/sn3218');

    async function get5volt() {
        gpio.write(pins.gpio.relay1, 1);
        await utils.sleep(300);
        return await ads1015.read(pins.adc.analog1);
    }

    async function get0volt() {
        gpio.write(pins.gpio.relay1, 0);
        await utils.sleep(300);
        return await ads1015.read(pins.adc.analog1);
    }

    async function get3count() {
        let result;

        for (let i = 0; i < 6; i++) {
            if (i % 2 == 1) {
                result = await get5volt();
            } else {
                result = await get0volt();
            }
        }

        return await result;
    }

    describe('relay1 on => adc1', () => {
        it('returns >= 5.0 V', async () => {
            const result = await get5volt();
            const isOk = result.toFixed(2) >= 5.0;
            assert.strictEqual(isOk, true);
        });
    });

    describe('relay1 off => adc1', () => {
        it('returns <= 0.1 V', async () => {
            const result = await get0volt();
            const isOk = result.toFixed(2) <= 0.1;
            assert.strictEqual(isOk, true);
        });
    });

    describe('relay1 on => input1', () => {
        it('input1 = true', async () => {
            const result = await get5volt();
            assert.strictEqual(gpio.read(pins.gpio.input1).value, true);
        });
    });

    describe('relay1 off => input1', () => {
        it('input1 = false', async () => {
            const result = await get0volt();
            assert.strictEqual(gpio.read(pins.gpio.input1).value, false);
        });
    });

    describe('relay1 on|off x 3 => input1', () => {
        it('input1.count = 3', async () => {
            gpio.resetCounter(pins.gpio.input1);
            const result = await get3count();
            assert.strictEqual(gpio.read(pins.gpio.input1).count, 3);
        });
    });
});

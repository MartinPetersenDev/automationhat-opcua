/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

describe('ADS1015 test', function () {
    const ads1015 = require('../src/sn3218');

    it('read() throws error when passed invalid input 5', function () {
        assert.throws(function () {
            ads1015.read(5);
        }, Error);
    });

    it('read() throws error when passed invalid input -1', function () {
        assert.throws(function () {
            ads1015.read(-1);
        }, Error);
    });

});

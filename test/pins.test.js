/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

describe('Pin definitions test', function () {
    // Pinout reference from https://github.com/pimoroni/automation-hat/blob/master/library/automationhat/__init__.py

    const pins = require('../src/pins');

    it('pins.led.adc1', function () {
        assert.strictEqual(pins.led.adc1, 1);
    });

    it('pins.led.adc2', function () {
        assert.strictEqual(pins.led.adc2, 2);
    });

    it('pins.led.adc3', function () {
        assert.strictEqual(pins.led.adc3, 3);
    });

    it('pins.led.output1', function () {
        assert.strictEqual(pins.led.output1, 4);
    });

    it('pins.led.output2', function () {
        assert.strictEqual(pins.led.output2, 5);
    });
    it('pins.led.output3', function () {
        assert.strictEqual(pins.led.output3, 6);
    });

    it('pins.led.relay1no', function () {
        assert.strictEqual(pins.led.relay1no, 7);
    });

    it('pins.led.relay1nc', function () {
        assert.strictEqual(pins.led.relay1nc, 8);
    });

    it('pins.led.relay2no', function () {
        assert.strictEqual(pins.led.relay2no, 9);
    });

    it('pins.led.relay2nc', function () {
        assert.strictEqual(pins.led.relay2nc, 10);
    });

    it('pins.led.relay3no', function () {
        assert.strictEqual(pins.led.relay3no, 11);
    });

    it('pins.led.relay3nc', function () {
        assert.strictEqual(pins.led.relay3nc, 12);
    });

    it('pins.led.input1', function () {
        assert.strictEqual(pins.led.input1, 15);
    });

    it('pins.led.input2', function () {
        assert.strictEqual(pins.led.input2, 14);
    });

    it('pins.led.input3', function () {
        assert.strictEqual(pins.led.input3, 13);
    });

    it('pins.led.warn', function () {
        assert.strictEqual(pins.led.warn, 16);
    });

    it('pins.led.comms', function () {
        assert.strictEqual(pins.led.comms, 17);
    });

    it('pins.led.power', function () {
        assert.strictEqual(pins.led.power, 18);
    });

    it('pins.gpio.relay1', function () {
        assert.strictEqual(pins.gpio.relay1, 13);
    });

    it('pins.gpio.relay2', function () {
        assert.strictEqual(pins.gpio.relay2, 19);
    });

    it('pins.gpio.relay3', function () {
        assert.strictEqual(pins.gpio.relay3, 16);
    });

    it('pins.gpio.output1', function () {
        assert.strictEqual(pins.gpio.output1, 5);
    });

    it('pins.gpio.output2', function () {
        assert.strictEqual(pins.gpio.output2, 12);
    });

    it('pins.gpio.output3', function () {
        assert.strictEqual(pins.gpio.output3, 6);
    });

    it('pins.gpio.input1', function () {
        assert.strictEqual(pins.gpio.input1, 26);
    });

    it('pins.gpio.input2', function () {
        assert.strictEqual(pins.gpio.input2, 20);
    });

    it('pins.gpio.input3', function () {
        assert.strictEqual(pins.gpio.input3, 21);
    });
});
